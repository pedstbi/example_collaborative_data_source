# Example Collaborative Data Source
A *fictitious* data set to be used in an example project set up.  This is a
supplement for DeWitt, P. and Bennett T. (2017) "Collaborative Reproducible
Reporting: Git Submodules as a Data Security Solution", a paper presented at
HEALTHINF 2017, the 10th International Conference on Health Informatics.


## Directory Contents

* README.md: this file
* generate-patient-data.R: The R script used to generate the example data set.
  This file is a stand in for the files/scripts/notes required for data base
  extracts, or other process that would provide the research team with .csv
  files for analysis.
* patient-data.csv: the example data set.
