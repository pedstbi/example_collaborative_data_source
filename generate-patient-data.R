# file: generate-patient-data.R
#
# purpose: generate a random data set an save it as a .csv.  This script would
# be the equivalent of a webscraper, database export, or some other process
# aimed and generating a .csv, or similar, file to be sent to an analyst.
#
# This script generates the data-raw/patient-data.csv file.

set.seed(42)
library(dplyr)
library(magrittr)
library(lubridate)

# number of subjects to create
N <- 4500

# Generate 8 digit long subject ids
pid <- 
  runif(N) %>% 
  as.character %>% 
  substr(., 3, 11)

# test
if (dplyr::n_distinct(pid) != N) stop("Non unique pids created")

# random site, denote as site site1, site2, ..., site15
site <- sample(paste0("site", 1:15), N, replace = TRUE)

# random admission date
admission_date <- 
  lubridate::mdy("01/01/1995") + 
  lubridate::days(sample(1:3650, N, replace = TRUE))

# random date of birth, needs to be before admission date, ages between 0 and 18
# years
dob <- admission_date - lubridate::days(sample(1:6574, N, replace = TRUE))

# sex
sex <- sample(c("Male", "Female"), N, replace = TRUE)

# length of stay
los <- rpois(N, 5.6)

# procedure codes
possible_codes <- runif(50, 80, 800) %>% formatC(format = "f", digits = 2)

# each subject will have between 10 and 50 codes
possible_codes <- 
  replicate(N, 
            sample(possible_codes, floor(runif(1, 10, 51)), replace = TRUE), 
            simplify = FALSE)

codes <- 
  lapply(possible_codes, 
         function(x) setNames(x, paste0("proc", seq(1, length(x), by = 1)))) 

code_dos <- 
  mapply(
         function(x, y) { 
           setNames(sample(seq(0, max(0, y - 1), by = 1), length(x), 
                           replace = TRUE),
                    paste0("procdos", seq(1, length(x), by = 1)))
         },
         x = codes, 
         y = as.list(los),
         SIMPLIFY = FALSE) %>% 
  lapply(as.list) %>%
  lapply(data.frame) %>%
  bind_rows

codes <- codes %>%
  lapply(as.list) %>%
  lapply(data.frame) %>%
  bind_rows

# disposition
disposition <- 
  sample(c("Home with Service", 
           "Home without Services", 
           "Dischaged/Transfered to Hospice", 
           "Discharged/Transfered to Rehab", 
           "Expired", 
           "Discontinued Care"),
         N, 
         replace = TRUE)

# Abbreviated Injury Score
headais <- sample(c(1:6, -9), N, prob = c(1, 2, 4, 9, 6, 1, 2), replace = TRUE)
neckais <- sample(c(1:6, -9), N, prob = c(1, 2, 4, 9, 6, 1, 2), replace = TRUE)

# systallic blood pressure
sbp <- rnorm(N, 120, 30) %>% floor

# build the data set and save as a .csv
egdata <- 
  data_frame(pid, 
             sex, 
             dob,
             site,
             admission_date, 
             los, 
             headais, 
             neckais, 
             sbp, 
             disposition) %>% 
  cbind(., codes, code_dos)

write.csv(egdata, file = "patient-data.csv", row.names = FALSE)
